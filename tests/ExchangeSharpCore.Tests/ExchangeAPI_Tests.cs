using jaq316.ExchangeSharpCore;
using Shouldly;
using System;
using Xunit;

namespace ExchangeSharpCore.Tests
{
    public class ExchangeAPI_Tests
    {
        [Fact]
        public void GetExchangeAPIDictionary_Should_Contain_Exchanges()
        {
            var exchanges = ExchangeAPI.GetExchangeAPIDictionary();

            exchanges.Count.ShouldBeGreaterThan(0);
        }
    }
}
